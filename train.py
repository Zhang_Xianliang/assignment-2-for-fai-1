import os
import time

import torch
import torch.nn as nn
import torchvision

from models import CNN, MyNet, MyNet_v4

# whether use gpu
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    CUDA = True
else:
    device = torch.device("cpu")
    CUDA = False

print(CUDA)

# Hyper Parameters
# model = CNN().to(device)
# model = MyNet().to(device)
model = MyNet_v4().to(device)
MAX_EPOCH = 200
BATCH_SIZE = 32
LR = 0.0001  # learning rate

optimizer = torch.optim.Adam(model.parameters(), lr=LR)
loss_func = nn.CrossEntropyLoss()  # the target label is not one-hotted
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=30, gamma=0.5)
model.train()

DOWNLOAD = False
# whether download cifar10 dataset
PATH = "../datasets/CIFAR10"
if not os.path.exists(PATH) :
    DOWNLOAD = True

# (0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)
# # (0.5,0.5,0.5),(0.5,0.5,0.5)
# transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor(),
#                                             torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
transform_aug = torchvision.transforms.Compose(
    [torchvision.transforms.RandomHorizontalFlip(p=0.5),  
     # torchvision.transforms.RandomAffine(degrees=0.2, scale=(0.8,1.2)),  
     torchvision.transforms.ToTensor(),  # torch.Tensor
     torchvision.transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),  
     torchvision.transforms.RandomErasing(p=0.5, scale=(0.02, 0.33), ratio=(0.3, 3.3), value=0)  
     ])
# load data
train_data = torchvision.datasets.CIFAR10(root=PATH,
                                          train=True, transform=transform_aug, download=DOWNLOAD)
classes = train_data.classes
print(classes)
train_loader = torch.utils.data.DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True)



for epoch in range(MAX_EPOCH):  # loop over the dataset multiple times
    running_loss = 0.0
    tic = time.time()
    for i, (inputs, labels) in enumerate(train_loader):
        if CUDA:
            inputs = inputs.cuda()
            labels = labels.cuda()
        # forward
        outputs = model(inputs)
        loss = loss_func(outputs, labels)
        # zero the parameter gradients
        optimizer.zero_grad()
        # backward
        loss.backward()
        # optimize
        optimizer.step()
        # print statistics
        running_loss += loss.item()
        if i % 500 == 499:  # print log
            print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / 500))
            running_loss = 0.0
    scheduler.step()
    toc = time.time()
    print("Used {:.2f}s to train one epoch.".format(toc - tic))

PATH = './model_saved.pth'
torch.save(model.state_dict(), PATH)
print('Finished Training And Results have SAVED in model_saved.pth')
