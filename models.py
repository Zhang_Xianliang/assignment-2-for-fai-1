import torch.nn as nn
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
import torchvision
import torch.nn.functional as F




class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Sequential(         # input shape (N, 3, 32, 32)
            nn.Conv2d(
                in_channels=3,              # input height
                out_channels=6,            # n_filters
                kernel_size=5,              # filter size
                stride=1,                   # filter movement/step
                padding=0,                  # if want same size after Conv2d, padding=(kernel_size-1)/2 if stride=1
            ),                              # output shape (N, 6, 28, 28)
            nn.BatchNorm2d(6),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(kernel_size=2),    # choose max value in 2x2 area, output shape (N, 6, 14, 14)
        )
        self.conv2 = nn.Sequential(         # input shape (N, 6, 14, 14)
            nn.Conv2d(6, 16, 5, 1, 0),     # output shape (N, 16, 10, 10)
            nn.BatchNorm2d(16),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 16, 5, 5)
        )

        self.fc1 = nn.Sequential(
            nn.Linear(16 * 5 * 5, 120),
            nn.ReLU(),
            nn.Dropout(p=0.5)

        )
        self.fc2 = nn.Sequential(
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Dropout(p=0.5)
        )
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):  # N, 3, 32, 32
        x = self.conv1(x)  # output shape (N, 6, 14, 14)
        x = self.conv2(x)  # output shape (N, 16, 5, 5)
        x = x.view(x.size(0), -1)  # output shape (N, 16 * 5 * 5)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x


class MyNet(nn.Module):
    def __init__(self):
        super(MyNet, self).__init__()
        # output = (input + 2 * padding - kernel) / stride + 1
        self.conv1 = nn.Sequential(         # input shape (N, 3, 32, 32)
            nn.Conv2d(
                in_channels=3,              # input height
                out_channels=24,            # n_filters
                kernel_size=5,             # filter size
                stride=1,                   # filter movement/step
                padding=2,                  # if want same size after Conv2d, padding=(kernel_size-1)/2 if stride=1
            ),                              # output shape (N, 24, 32, 32)
            nn.BatchNorm2d(24),
            nn.ReLU(),                      # activation
        )
        self.conv2 = nn.Sequential(         # input shape (N, 24, 32, 32)
            nn.Conv2d(24, 64, 5, 1, 2),     # output shape (N, 64, 32, 32)
            nn.BatchNorm2d(64),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 64, 16, 16)
        )
        self.conv3 = nn.Sequential(         # input shape (N, 64, 16, 16)
            nn.Conv2d(64, 96, 3, 1, 1),     # output shape (N, 96, 16, 16)
            nn.BatchNorm2d(96),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 96, 8, 8)
        )
        self.conv4= nn.Sequential(          # input shape (N, 96, 8, 8)
            nn.Conv2d(96, 96, 3, 1, 1),   # output shape (N, 96, 8, 8)
            nn.BatchNorm2d(96),
            nn.ReLU(),                      # activation
        )
        self.conv5= nn.Sequential(          # input shape (N, 96, 8, 8)
            nn.Conv2d(96, 64, 3, 1, 1),   # output shape (N, 64, 8, 8)
            nn.BatchNorm2d(64),
            nn.ReLU(),                      # activation
            
        )

        self.fc1 = nn.Sequential(
            nn.Linear(64 * 8 * 8, 64 * 8 * 8),
            nn.ReLU(),
            nn.Dropout(p=0.5),

            nn.Linear(64 * 8 * 8, 250),
            nn.ReLU(),
            nn.Dropout(0.5), 

            nn.Linear(250, 25),
            nn.ReLU(),
            nn.Dropout(0.5), 
        )
        self.fc2 = nn.Linear(25, 10)

    def forward(self, x):  # N, 3, 32, 32
        x = self.conv1(x)  # output shape (N, 24, 32, 32)
        x = self.conv2(x)  # output shape (N, 64, 16, 16)
        x = self.conv3(x)  # output shape (N, 96, 8, 8)
        x = self.conv4(x)  # output shape (N, 96, 8, 8)
        x = self.conv5(x)  # output shape (N, 64, 8, 8)
        x = x.view(x.size(0), -1)  # output shape (N, 64 * 8*  8)
        x = self.fc1(x)  # output shape (N, 25)
        x = self.fc2(x)  # output shape (N, 10)
        return x


class MyNet_v4(nn.Module):
    def __init__(self):
        super(MyNet_v4, self).__init__()
        # output = (input + 2 * padding - kernel) / stride + 1
        self.conv1 = nn.Sequential(         # input shape (N, 3, 32, 32)
            nn.BatchNorm2d(3),              # acc=88.46% without first batch normalization
            nn.Conv2d(
                in_channels=3,              # input height
                out_channels=96,            # n_filters
                kernel_size=5,              # filter size
                stride=1,                   # filter movement/step
                padding=2,                  # if want same size after Conv2d, padding=(kernel_size-1)/2 if stride=1
            ),                              # output shape (N, 96, 32, 32)
            nn.BatchNorm2d(96),
            nn.ReLU(),                      # activation
        )
        self.conv2 = nn.Sequential(         # input shape (N, 96, 32, 32)
            nn.Conv2d(96, 256, 5, 1, 2),     # output shape (N, 256, 32, 32)
            nn.BatchNorm2d(256),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 256, 16, 16)
        )
        self.conv3 = nn.Sequential(         # input shape (N, 256, 16, 16)
            nn.Conv2d(256, 384, 3, 1, 1),     # output shape (N, 384, 16, 16)
            nn.BatchNorm2d(384),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 384, 8, 8)
        )
        self.conv4= nn.Sequential(          # input shape (N, 384, 8, 8)
            nn.Conv2d(384, 384, 3, 1, 1),   # output shape (N, 384, 8, 8)
            nn.BatchNorm2d(384),
            nn.ReLU(),                      # activation
        )
        self.conv5= nn.Sequential(          # input shape (N, 384, 8, 8)
            nn.Conv2d(384, 256, 3, 1, 1),   # output shape (N, 256, 8, 8)
            nn.BatchNorm2d(256),
            nn.ReLU(),                      # activation
        )

        self.fc1 = nn.Sequential(
            nn.Linear(256 * 8 * 8, 4096),
            nn.ReLU(),
            nn.Dropout(p=0.5),

            nn.Linear(4096, 1024),
            nn.ReLU(),
            nn.Dropout(0.5), 

            nn.Linear(1024, 250),
            nn.ReLU(),
            nn.Dropout(0.5), 

            nn.Linear(250, 25),
            nn.ReLU(),
            nn.Dropout(0.5), 
        )
        self.fc2 = nn.Linear(25, 10)

    def forward(self, x):  # N, 3, 32, 32
        x = self.conv1(x)  # output shape (N, 96, 32, 32)
        x = self.conv2(x)  # output shape (N, 256, 16, 16)
        x = self.conv3(x)  # output shape (N, 384, 8, 8)
        x = self.conv4(x)  # output shape (N, 384, 8, 8)
        x = self.conv5(x)  # output shape (N, 256, 8, 8)
        x = x.view(x.size(0), -1)  # output shape (N, 256 * 8*  8)
        x = self.fc1(x)  # output shape (N, 25)
        x = self.fc2(x)  # output shape (N, 10)
        return x



def show_example(images, labels, classes):
    image = torchvision.utils.make_grid(images).numpy().transpose(1, 2, 0)
    plt.imshow(image)
    plt.title(' '.join('%5s' % classes[labels[j]] for j in range(4)))
    plt.show()
