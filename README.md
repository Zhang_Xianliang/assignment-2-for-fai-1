# README #

------

#### Assignment two for 先端人工知能論Ⅰ／Frontier Artificial Intelligence I  

#### Theme: CIFAR-10 Classification with Pytorch  

#### Name: Zhang Xianliang 張賢亮  

#### Date: 2020/6/2 

Code Link: [bitbucket](https://Zhang_Xianliang@bitbucket.org/Zhang_Xianliang/assignment-2-for-fai-1.git)

> Or you can clone codes by using command:   
> 
> git clone https://Zhang_Xianliang@bitbucket.org/Zhang_Xianliang/assignment-2-for-fai-1.git

***Best test accuracy:  90.71%***

>try Dropout, Optimizer, Batch Normarization, Data Augmentation, Learning Rate Decay and so on

------

### File/Folder Infomation

| Name                                | Description                             |
| ------------------------------------| --------------------------------------- |
| README.md                           | README                                  |
| CNN.py                              | model defination                        |
| test.py                             | for test                                |
| train.py                            | for training                            |
| model_saved_\<accuracy>\_\<method>\_\<version>.pth | saved_models                            |

### Results of each model

---

| index | accuracy |
| :---: | :------: |
|   1   |   62.46%  |
|   2   |   78.03%  |
|   3   |   83.75%  |
|   4   |   87.08%  |
|   5   |   88.46%  |
|   5   |   90.71%  |
| best  |   90.71%  |

---



## Conclusion

>今回実験されたモデルのうち、一番高いaccuracy(90.71%)を達成したモデルのストラクチャはこんな感じです：
```python
        self.conv1 = nn.Sequential(         # input shape (N, 3, 32, 32)
            nn.BatchNorm2d(3),              # without this layer, acc will be 88.46%
            nn.Conv2d(
                in_channels=3,              # input height
                out_channels=96,            # n_filters
                kernel_size=5,              # filter size
                stride=1,                   # filter movement/step
                padding=2,                  # if want same size after Conv2d, padding=(kernel_size-1)/2 if stride=1
            ),                              # output shape (N, 96, 32, 32)
            nn.BatchNorm2d(96),
            nn.ReLU(),                      # activation
        )
        self.conv2 = nn.Sequential(         # input shape (N, 96, 32, 32)
            nn.Conv2d(96, 256, 5, 1, 2),     # output shape (N, 256, 32, 32)
            nn.BatchNorm2d(256),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 256, 16, 16)
        )
        self.conv3 = nn.Sequential(         # input shape (N, 256, 16, 16)
            nn.Conv2d(256, 384, 3, 1, 1),     # output shape (N, 384, 16, 16)
            nn.BatchNorm2d(384),
            nn.ReLU(),                      # activation
            nn.MaxPool2d(2),                # output shape (N, 384, 8, 8)
        )
        self.conv4= nn.Sequential(          # input shape (N, 384, 8, 8)
            nn.Conv2d(384, 384, 3, 1, 1),   # output shape (N, 384, 8, 8)
            nn.BatchNorm2d(384),
            nn.ReLU(),                      # activation
        )
        self.conv5= nn.Sequential(          # input shape (N, 384, 8, 8)
            nn.Conv2d(384, 256, 3, 1, 1),   # output shape (N, 256, 8, 8)
            nn.BatchNorm2d(256),
            nn.ReLU(),                      # activation
        )

        self.fc1 = nn.Sequential(
            nn.Linear(256 * 8 * 8, 4096),   # output shape (N, 4096)
            nn.ReLU(),
            nn.Dropout(p=0.5),

            nn.Linear(4096, 1024),          # output shape (N, 1024)
            nn.ReLU(),
            nn.Dropout(0.5), 

            nn.Linear(1024, 250),           # output shape (N, 250)
            nn.ReLU(),
            nn.Dropout(0.5), 

            nn.Linear(250, 25),             # output shape (N, 25)
            nn.ReLU(),
            nn.Dropout(0.5), 
        )
        self.fc2 = nn.Linear(25, 10)
```
>また、max_epoch数は200で、mini-batchのサイズは32で、optimizerはAdamで、学習率は0.001で、Dropoutのratioは0.5で, schedulerのstep_sizeは30で、gammaは0.5でした. 今回の実験によって、いろいろ勉強になりました。例えば、Adamを使うと、ネットワークの収束の速度は確実に早くにりました。DropoutやBatchNormarizationなどを使うと、過学習を抑えられました。Data Augmentationを使うと、正確率も確実に上がりました。
