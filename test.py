from models import CNN, MyNet, MyNet_v4
import torch.nn as nn
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
import torchvision

DATASET_PATH = "../datasets/CIFAR10"

transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor(),
                                            torchvision.transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5))])
test_data = torchvision.datasets.CIFAR10(root=DATASET_PATH,
                                       train=False, download=False, transform=transform)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=16,
                                         shuffle=False)
classes = test_data.classes
print(classes)
PATH = './model_saved_90.71_MyNet_v4.pth'
# model = CNN()
# model = MyNet()
model = MyNet_v4()
model.load_state_dict(torch.load(PATH, map_location='cpu'))
model.eval()

correct = 0
total = 0
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print('Accuracy of the network on the 10000 test images: %.2f %%' % (
    100 * correct / total))

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        outputs = model(images)
        _, predicted = torch.max(outputs, 1)
        c = (predicted == labels).squeeze()
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1


for i in range(10):
    print('Accuracy of %5s : %.2f %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))